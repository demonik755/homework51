import React from 'react';

import './Movie.css';

const Movies = (props) => {
    return (
        <div className="movies">
            <img src={props.image} alt="image"/>
            <h1>{props.name}</h1>
            <p>Year: {props.year}</p>
            <h2>Genre: {props.genre}</h2>
        </div>
    );
};

export default Movies;