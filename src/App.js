import React from 'react';

import snatch from './images/snatch.jpg';

import fightclub from './images/fightclub.jpg';

import Lotr from './images/LoTR.jpg';

import Movies from "./components/Movie/Movies";

import './App.css';



function App() {
  return (
    <div className="App">
        <Movies image={Lotr} name="The Lord of the Rings" year="2001—2003" genre=" Adventure, Drama, Fantasy"/>
        <Movies image ={fightclub} name="Fight Club " year="1999" genre="Drama"/>
        <Movies image={snatch} name="Snatch. " year="2000" genre=" Comedy, Crime"/>
    </div>
  );
}

export default App;
